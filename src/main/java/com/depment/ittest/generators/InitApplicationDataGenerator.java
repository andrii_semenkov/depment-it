package com.depment.ittest.generators;

import com.depment.ittest.database.*;
import com.depment.ittest.database.model.*;
import com.depment.ittest.security.LoginController;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.security.auth.Subject;
import java.security.Principal;

@Service
public class InitApplicationDataGenerator {
    final Long id1 = 1000L;
    final Long id2 = 1001L;
    final Long id3 = 1002L;
    final String key1 = "key1";
    final String key2 = "key2";
    final String key3 = "key3";

    private CategoryRepository categoryRepository;
    private PositionRepository positionRepository;
    private UserRepository userRepository;
    private final PriorityRepository priorityRepository;
    private final EnvironmentRepository environmentRepository;
    private final ToDoListRepository toDoListRepository;
    private final ExtensionsRepository extensionsRepository;
    private final LoginController loginController;

    @Autowired
    public InitApplicationDataGenerator(final CategoryRepository categoryRepository, final PositionRepository positionRepository, final UserRepository userRepository, final PriorityRepository priorityRepository, final EnvironmentRepository environmentRepository, final ToDoListRepository toDoListRepository, final ExtensionsRepository extensionsRepository, final LoginController loginController) {
        this.categoryRepository = categoryRepository;
        this.positionRepository = positionRepository;
        this.userRepository = userRepository;
        this.priorityRepository = priorityRepository;
        this.environmentRepository = environmentRepository;
        this.toDoListRepository = toDoListRepository;
        this.extensionsRepository = extensionsRepository;
        this.loginController = loginController;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void generateInitData() {
        loginController.login(new Principal(){
            @Override
            public String getName() {
                return "user";
            }

            @Override
            public boolean implies(final Subject subject) {
                return true;
            }
        });
        generatePositions();
        generateCategory();
        generateUsers();
        generatePriorities();
        generateEnvironments();
        generateToDos();
    }

    public void generateToDos() {
        for (int i = 0; i < 10; i++) {
            val todo1 = ToDoListItem.builder().text("test1" + i).checked(false).archived(false).build();
            toDoListRepository.saveAndFlush(todo1);
        }
    }

    public void generatePriorities() {
        val minor = Priority.builder().priorityName("Minor").build();
        val major = Priority.builder().priorityName("Major").build();
        val critical = Priority.builder().priorityName("Critical").build();
        priorityRepository.saveAndFlush(minor);
        priorityRepository.saveAndFlush(major);
        priorityRepository.saveAndFlush(critical);
    }

    public void generateEnvironments() {
        val chair = Environment.builder()
                .category(categoryRepository.findByCategoryName("Chair").get())
                .user(userRepository.findByEmail("klaus.flaus@mail.ru").get())
                .description("Chair Ikea Nowy Styl")
                .nkey("nkey1")
                .build();
        val laptop = Environment.builder()
                .category(categoryRepository.findByCategoryName("Laptop").get())
                .user(userRepository.findByEmail("andrii.semenkov@mail.ru").get())
                .nkey("nkey2")
                .description("Laptop DELL E8870")
                .build();
        val pc = Environment.builder()
                .category(categoryRepository.findByCategoryName("PC").get())
                .user(userRepository.findByEmail("p.piotr@mail.ru").get())
                .nkey("nkey3")
                .description("PC DELL i7-8700K")
                .build();
        val pc2 = Environment.builder()
                .category(categoryRepository.findByCategoryName("PC").get())
                .nkey("nkey4")
                .description("PC DELL i7-8800K")
                .build();
        val pc3 = Environment.builder()
                .category(categoryRepository.findByCategoryName("PC").get())
                .nkey("nkey4")
                .description("PC DELL i5-8500K")
                .build();

        for (int i = 4; i < 10; i++) {
            final Environment build = Environment.builder().category(categoryRepository.findByCategoryName("PC").get()).nkey("nkey" + i).description("PC DELL i5-8500K RandomPC: " + i).build();
            environmentRepository.saveAndFlush(build);
        }
        environmentRepository.saveAndFlush(chair);
        environmentRepository.saveAndFlush(laptop);
        environmentRepository.saveAndFlush(pc);
        environmentRepository.saveAndFlush(pc2);
        environmentRepository.saveAndFlush(pc3);
    }

    public void generateUsers() {
        final String password = "{noop}password";
        val userPassword = User.builder()
                .email("user")
                .password(password)
                .userName("user")
                .employeeKey("user")
                .surname("user")
                .position(positionRepository.findByPositionName("Manager").get())
                .build();
        val manager = User.builder()
                .position(positionRepository.findByPositionName("Manager").get())
                .password(password)
                .userName("Klaus Flaus")
                .email("klaus.flaus@mail.ru")
                .id(id1)
                .employeeKey(key1)
                .build();
        val developer = User.builder()
                .position(positionRepository.findByPositionName("Developer").get())
                .password(password)
                .userName("Andrii Semenkov")
                .email("andrii.semenkov@mail.ru")
                .id(id2)
                .employeeKey(key2)
                .build();

        val hr = User.builder()
                .position(positionRepository.findByPositionName("HR").get())
                .password(password)
                .userName("Piotr Piotrowski")
                .email("p.piotr@mail.ru")
                .id(id3)
                .employeeKey(key3)
                .build();
        userRepository.saveAndFlush(developer);
        userRepository.saveAndFlush(hr);
        userRepository.saveAndFlush(manager);
        userRepository.saveAndFlush(userPassword);
        userRepository.flush();
    }

    public void generateCategory() {
        val laptop = Category.builder().id(id1).categoryName("Laptop").build();
        val PC = Category.builder().id(id2).categoryName("PC").build();
        val chair = Category.builder().id(id1).categoryName("Chair").build();
        categoryRepository.saveAndFlush(laptop);
        categoryRepository.saveAndFlush(PC);
        categoryRepository.saveAndFlush(chair);
        categoryRepository.flush();

        val color = CategoryExtension.builder()
                .category(categoryRepository.findByCategoryName("Laptop").get())
                .extensionKey("Color")
                .build();
        val size = CategoryExtension.builder()
                .category(categoryRepository.findByCategoryName("PC").get())
                .extensionKey("Size")
                .build();
        val height = CategoryExtension.builder()
                .category(categoryRepository.findByCategoryName("Chair").get())
                .extensionKey("190")
                .build();

        extensionsRepository.saveAndFlush(color);
        extensionsRepository.saveAndFlush(size);
        extensionsRepository.saveAndFlush(height);
    }


    public void generatePositions() {
        val manager = Position.builder().id(id1).positionName("Manager").description("Manager position, responsible for some management staff.").build();

        val developer = Position.builder().id(id2).positionName("Developer").description("Developer position.").build();

        val hr = Position.builder().id(id3).positionName("HR").description("HR. Responsible for finding people.").build();
        positionRepository.saveAndFlush(manager);
        positionRepository.saveAndFlush(developer);
        positionRepository.saveAndFlush(hr);
        positionRepository.flush();
    }


}
