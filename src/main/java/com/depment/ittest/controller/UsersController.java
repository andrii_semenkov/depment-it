package com.depment.ittest.controller;

import com.depment.ittest.api.UserSelectableDTO;
import com.depment.ittest.database.UserRepository;
import com.depment.ittest.database.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/users")
public class UsersController extends AbstractDepmentRepositoryController<User> {
    private final UserRepository userRepository;

    @Autowired
    public UsersController(final UserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }


    @RequestMapping("/getAllAsSelectable")
    public List<UserSelectableDTO> getAllSelectable() {
        return repository.findAll().stream().map(user -> new UserSelectableDTO(user.getId(), user.getEmail())).collect(Collectors.toList());
    }

    @RequestMapping("/findByName")
    public User findByName(@RequestParam(name = "name") String name) {
        return userRepository.findByEmailOrUserName(name, name).orElse(null);
    }
}
