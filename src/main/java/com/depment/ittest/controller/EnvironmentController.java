package com.depment.ittest.controller;

import com.depment.ittest.api.EnvironmentSelectableDTO;
import com.depment.ittest.api.FlatEnvironment;
import com.depment.ittest.database.EnvironmentRepository;
import com.depment.ittest.database.model.Environment;
import com.depment.ittest.database.model.Ticket;
import com.depment.ittest.service.ReportingService;
import com.depment.ittest.service.UserEnvironmentAssignmentService;
import com.depment.ittest.transformer.EnvironementFlatTransformer;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping(path = "/api/environments")
public class EnvironmentController extends AbstractDepmentRepositoryController<Environment> {
    private final UserEnvironmentAssignmentService assignmentService;
    private final ReportingService reportingService;
    private final EnvironmentRepository environmentRepository;
    private final Function<Environment, EnvironmentSelectableDTO> mapper = environment -> new EnvironmentSelectableDTO(environment.getId(), environment.getDescription());


    @Autowired
    public EnvironmentController(final EnvironmentRepository repository, final UserEnvironmentAssignmentService assignmentService, final ReportingService reportingService) {
        super(repository);
        this.environmentRepository = repository;
        this.assignmentService = assignmentService;
        this.reportingService = reportingService;
    }

    @RequestMapping("/finAllEnvironmentsFlatted")
    public List<FlatEnvironment> finAllEnvironmentsFlatted() {
        return findAll().stream().map(EnvironementFlatTransformer::toFlat).collect(Collectors.toList());
    }

    @RequestMapping("/create")
    public Environment create(@RequestBody Environment environment) {
        return super.add(environment);
    }

    @RequestMapping("/addToUser")
    public void addEnvironmentToUser(@RequestBody Long userId, @RequestBody Long environmentId) {
        assignmentService.addToUser(userId, environmentId);
    }

    @RequestMapping("/addEnvironmentsToUser")
    public void adEnvironmentsToUser(@RequestParam String userId, @RequestBody(required = false) String[] environmentIds) {

        final List<Long> envIds = Stream.of(environmentIds).map(Long::decode).collect(Collectors.toList());
        assignmentService.addToUser(Long.decode(userId), envIds);
    }

    @RequestMapping("/unAssignEnvironment")
    public void unAssignEnvironment(@RequestBody Long envId) {
        assignmentService.unAssignEnvironment(envId);
    }

    @RequestMapping("/getAllAsSelectable")
    public List<EnvironmentSelectableDTO> getAllAsSelectable() {
        return repository.findAll().stream().map(mapper).collect(Collectors.toList());
    }

    @RequestMapping("/findEnvironmentByUserId")
    public List<EnvironmentSelectableDTO> findEnvironmentByUserId(@RequestBody Long userId) {
        return environmentRepository.findByUser_Id(userId).stream().map(mapper).collect(Collectors.toList());
    }

    @RequestMapping("/getMyEnvironments")
    @Transactional(readOnly = true)
    public List<Environment> getMyEnvironments(@RequestParam() String userName) {
        return environmentRepository.findByUser_UserName(userName);
    }

    @RequestMapping("/reportProblem")
    public void reportProblem(@RequestParam() String userEmail, @RequestParam() String environmentIds, @RequestBody() String body) {
        val string = StringUtils.splitByWholeSeparator(environmentIds, ";");
        final List<Long> longs = Stream.of(string).map(Long::parseLong).collect(Collectors.toList());
        reportingService.reportEnvironmentProblem(userEmail, longs, body);
    }

    @RequestMapping("/findAllReported")
    public List<Ticket> findAllReported(@RequestParam() String userEmail) {
        Validate.notNull(userEmail);
        return reportingService.findAllReportedForUser(userEmail);
    }

    @RequestMapping("/findAllTickets")
    public List<Ticket> findAllTickets() {
        return reportingService.findAll();
    }

    @RequestMapping("/saveTickets")
    public void saveTickets(List<Ticket> tickets) {
        this.reportingService.saveTickets(tickets);
    }

    @RequestMapping("/saveSolution")
    public void saveSolution(@RequestParam Long id, @RequestBody String solution) {
        this.reportingService.saveSolution(id, solution);
    }

    @RequestMapping("/countAllReported")
    public Long countAllReported() {
        return reportingService.countAllReported();
    }
}
