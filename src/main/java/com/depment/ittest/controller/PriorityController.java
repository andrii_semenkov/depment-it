package com.depment.ittest.controller;

import com.depment.ittest.database.PriorityRepository;
import com.depment.ittest.database.model.Priority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/priority")
public class PriorityController extends AbstractDepmentRepositoryController<Priority> {
    @Autowired
    public PriorityController(final PriorityRepository repository) {
        super(repository);
    }
}
