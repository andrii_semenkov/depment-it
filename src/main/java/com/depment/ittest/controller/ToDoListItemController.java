package com.depment.ittest.controller;

import com.depment.ittest.database.model.ToDoListItem;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/todolist")
public class ToDoListItemController extends AbstractDepmentRepositoryController<ToDoListItem> {
    @Autowired
    public ToDoListItemController(final JpaRepository<ToDoListItem, Long> repository) {
        super(repository);
    }

    @RequestMapping("/archive")
    public void archiveTodo(@RequestParam() Long id) {
        final Optional<ToDoListItem> item = repository.findById(id);
        if (item.isPresent()) {
            final ToDoListItem toDoListItem = item.get();
            toDoListItem.setArchived(true);
            repository.saveAndFlush(toDoListItem);
        }
    }

    @RequestMapping("/changeState")
    public void changeState(@RequestParam() Long id, @RequestParam() Boolean state) {
        final Optional<ToDoListItem> item = repository.findById(id);
        if (item.isPresent()) {
            final ToDoListItem toDoListItem = item.get();
            toDoListItem.setChecked(state);
            repository.saveAndFlush(toDoListItem);
        }
    }

    @RequestMapping("/getAllActive")
    public List<ToDoListItem> getAllActive() {
        val item = ToDoListItem.builder().archived(false).build();
        Example example = Example.of(item);
        return repository.findAll(example);
    }

    @RequestMapping("/addNewItem")
    public void add(@RequestBody() String text) {
        final ToDoListItem toDoListItem = ToDoListItem.builder().archived(false).checked(false).text(text).build();
        super.add(toDoListItem);
    }
}
