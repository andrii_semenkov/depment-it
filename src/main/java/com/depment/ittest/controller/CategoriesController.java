package com.depment.ittest.controller;

import com.depment.ittest.database.CategoryRepository;
import com.depment.ittest.database.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Transactional
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/categories")
public class CategoriesController {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoriesController(final CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @PostMapping(path = "/add")
    public Category add(@RequestBody Category category) {
        return this.categoryRepository.save(category);
    }

    @DeleteMapping(path = {"delete/{id}"})
    public void delete(@PathVariable(value = "id") Long id) {
        this.categoryRepository.deleteById(id);
    }

    @RequestMapping(path = "/getAll")
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @RequestMapping(path = "/findByName")
    public Category findByName(@RequestParam String categoryName) {
        return categoryRepository.findByCategoryName(categoryName).orElse(null);
    }
}
