package com.depment.ittest.controller.mobile;

import com.depment.ittest.controller.CategoriesController;
import com.depment.ittest.database.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "mobile/api/categories")
public class CategoriesMobileController extends CategoriesController {
    @Autowired
    public CategoriesMobileController(final CategoryRepository categoryRepository) {
        super(categoryRepository);
    }
}
