package com.depment.ittest.controller;

import com.depment.ittest.database.model.EnvironmentRequest;
import com.depment.ittest.service.interfaces.EnvironmentRequestService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/request/")
public class RequestController {
    private final EnvironmentRequestService environmentRequestService;

    public RequestController(final EnvironmentRequestService environmentRequestService) {
        this.environmentRequestService = environmentRequestService;
    }

    @RequestMapping("createRequest")
    public EnvironmentRequest createRequest(@RequestParam() Long environmentId, @RequestParam() String userEmail, @RequestBody() String requestReason) {
        return this.environmentRequestService.createRequest(environmentId, userEmail, requestReason);
    }

    @RequestMapping("acceptRequest")
    public void acceptRequest(@RequestParam() Long requestId) {
        this.environmentRequestService.acceptRequest(requestId);
    }

    @RequestMapping("declineRequest")
    public void declineRequest(@RequestParam() Long requestId, @RequestBody(required = false) String declineReason) {
        this.environmentRequestService.declineRequest(requestId, declineReason);
    }

    @RequestMapping("findForUser")
    public List<EnvironmentRequest> findForUser(@RequestParam() String userEmail) {
        return this.environmentRequestService.findByUserEmail(userEmail);
    }
}
