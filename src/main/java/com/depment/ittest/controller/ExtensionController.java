package com.depment.ittest.controller;

import com.depment.ittest.api.CategorySelectableDTO;
import com.depment.ittest.database.CategoryRepository;
import com.depment.ittest.database.model.Category;
import com.depment.ittest.database.model.CategoryExtension;
import com.depment.ittest.database.model.CategoryExtensionValue;
import com.depment.ittest.service.interfaces.ExtensionsManager;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/extension/")
public class ExtensionController {
    private final ExtensionsManager extensionsManager;
    private final CategoryRepository categoryRepository;

    public ExtensionController(final ExtensionsManager extensionsManager, final CategoryRepository categoryRepository) {
        this.extensionsManager = extensionsManager;
        this.categoryRepository = categoryRepository;
    }

    @RequestMapping("createExtensionKeyForCategory")
    public void createExtensionKeyForCategory(@RequestParam() Long categoryId, @RequestParam() String extensionKey) {
        this.extensionsManager.saveEmptyExtension(categoryId, extensionKey);
    }

    @RequestMapping("createValueForExtension")
    public void createValueForExtension(@RequestParam() Long extensionId, @RequestBody() String extensionValue) {
        this.extensionsManager.updateExtensionValue(extensionId, CategoryExtensionValue.of(extensionValue));
    }

    @RequestMapping("findExtensionsByCategory")
    public List<CategoryExtension> findExtensionsByCategory(@RequestParam() String categoryName) {
        return extensionsManager.findAllByCategoryName(categoryName);
    }

    @RequestMapping("findSelectableAllSelectableCategories")
    public List<CategorySelectableDTO> findSelectableAllSelectableCategories() {
        return categoryRepository.findAll().stream().map(this::transformToNgSelect).collect(Collectors.toList());
    }

    private CategorySelectableDTO transformToNgSelect(final Category cat) {
        return new CategorySelectableDTO(cat.getId().toString(), cat.getCategoryName());
    }
}
