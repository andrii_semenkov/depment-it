package com.depment.ittest.controller;

import com.depment.ittest.database.JournalRepository;
import com.depment.ittest.database.model.Journal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/journal")
public class JournalController extends AbstractDepmentRepositoryController<Journal> {
    @Autowired
    public JournalController(final JournalRepository repository) {
        super(repository);
    }
}
