package com.depment.ittest.controller;

import com.depment.ittest.database.PositionRepository;
import com.depment.ittest.database.model.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/positions")
public class PositionsController extends AbstractDepmentRepositoryController<Position> {
    @Autowired
    public PositionsController(final PositionRepository positionRepository) {
        super(positionRepository);
    }
}
