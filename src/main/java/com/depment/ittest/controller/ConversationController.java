package com.depment.ittest.controller;

import com.depment.ittest.database.TicketsRepository;
import com.depment.ittest.database.model.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/conversation")
public class ConversationController extends AbstractDepmentRepositoryController<Ticket> {

    @Autowired
    public ConversationController(final TicketsRepository repository) {
        super(repository);
    }
}
