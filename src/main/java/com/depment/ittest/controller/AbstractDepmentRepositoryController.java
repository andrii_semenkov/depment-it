package com.depment.ittest.controller;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Transactional
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public abstract class AbstractDepmentRepositoryController<Entity> {
    protected JpaRepository<Entity, Long> repository;

    public AbstractDepmentRepositoryController(JpaRepository<Entity, Long> repository) {
        this.repository = repository;
    }

    @PostMapping(path = "/add")
    public Entity add(@RequestBody Entity entity) {
        return repository.saveAndFlush(entity);
    }

    @DeleteMapping(path = {"delete/{id}"})
    public void delete(@PathVariable(value = "id") Long id) {
        repository.deleteById(id);
    }

    @RequestMapping(path = "/getAll")
    public List<Entity> getAll() {
        return repository.findAll();
    }

    @RequestMapping(path = "/findAll")
    public List<Entity> findAll() {
        return repository.findAll();
    }


    @RequestMapping(path = "/findById")
    public Entity findById(@RequestBody Long id) {
        return repository.findById(id).orElse(null);
    }

    @RequestMapping(path = "/findByIds")
    public List<Entity> findByIds(@RequestBody List<Long> ids) {
        return repository.findAllById(ids);
    }
}
