package com.depment.ittest;

import com.depment.ittest.database.model.EnvironmentRequest;
import com.depment.ittest.database.model.Ticket;
import com.depment.ittest.database.model.User;
import lombok.val;
import org.springframework.data.domain.Example;

import java.util.List;

public class StupidWorkAroundDispatcher {
    public static List<Example<Ticket>> createForTicket(String emailAndName) {
        val candidate1 = Example.of(Ticket.builder().user(User.builder().email(emailAndName).build()).build());
        val candidate2 = Example.of(Ticket.builder().user(User.builder().userName(emailAndName).build()).build());
        return io.vavr.collection.List.of(candidate1, candidate2).toJavaList();
    }

    public static List<Example<EnvironmentRequest>> createForUser(String emailAndName) {
        final EnvironmentRequest environmentRequest = EnvironmentRequest.builder().user(User.builder().email(emailAndName).build()).build();
        val candidate1 = Example.of(environmentRequest);
        final EnvironmentRequest environmentRequest2 = EnvironmentRequest.builder().user(User.builder().userName(emailAndName).build()).build();
        val candidate2 = Example.of(environmentRequest2);
        return io.vavr.collection.List.of(candidate1, candidate2).toJavaList();
    }
}
