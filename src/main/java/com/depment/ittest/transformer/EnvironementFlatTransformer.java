package com.depment.ittest.transformer;

import com.depment.ittest.api.FlatEnvironment;
import com.depment.ittest.database.model.Environment;

import java.util.Objects;

import static java.util.Objects.isNull;

public class EnvironementFlatTransformer {
    public static FlatEnvironment toFlat(Environment environment) {
        return FlatEnvironment.builder()
                .id(environment.getId())
                .categoryExtensionValues(environment.getCategoryExtensionValues())
                .description(environment.getDescription())
                .categoryName(environment.getCategory().getCategoryName())
                .nkey(environment.getNkey())
                .validityPeriod(environment.getValidityPeriod())
                .userEmail(!isNull(environment.getUser()) ? environment.getUser().getEmail() : "Not assigned")
                .build();
    }
}
