package com.depment.ittest.api;

import com.depment.ittest.database.model.CategoryExtensionValue;
import lombok.Builder;
import lombok.Value;

import java.time.Period;
import java.util.List;

@Value
@Builder
public class FlatEnvironment {
    private Long id;
    private String nkey;
    private String description;
    private String userEmail;
    private String categoryName;
    private Period validityPeriod;
    private List<CategoryExtensionValue> categoryExtensionValues;
}
