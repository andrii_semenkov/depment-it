package com.depment.ittest.api;

import lombok.Value;

@Value
public class UserSelectableDTO implements NgSelectDTO {
    private Long id;
    private String text;
}
