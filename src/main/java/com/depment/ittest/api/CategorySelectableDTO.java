package com.depment.ittest.api;

import lombok.Value;

@Value
public class CategorySelectableDTO implements NgSelectDTO {
    private String id;
    private String text;
}
