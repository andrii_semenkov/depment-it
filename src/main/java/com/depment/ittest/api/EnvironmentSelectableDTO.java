package com.depment.ittest.api;

import lombok.Value;

@Value
public class EnvironmentSelectableDTO implements NgSelectDTO {
    private Long id;
    private String text;
}
