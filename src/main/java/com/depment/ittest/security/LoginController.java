package com.depment.ittest.security;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(path = "/api")
public class LoginController {

    @RequestMapping("/login")
    public Principal login(Principal user) {
        return user;
    }
}
