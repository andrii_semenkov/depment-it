package com.depment.ittest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Controller
@EnableScheduling
@SpringBootApplication
public class ItTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(ItTestApplication.class, args);
    }


    @RequestMapping(value = "/")
    public String index() {
        return "index";
    }

//
//    @RequestMapping(value = "/health")
//    public void health() {
//
//    }
//
//    @RequestMapping(value = "/mobile/test")
//    public String mobileTest() {
//        return "Hello mobile app!";
//    }
//


    @Configuration
    @Order(SecurityProperties.BASIC_AUTH_ORDER)
    protected static class SecurityConfiguration extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.httpBasic().and().authorizeRequests().antMatchers("/api/**").authenticated().and().formLogin().loginPage("/login");
            http.httpBasic().and().authorizeRequests().antMatchers("/", "/login", "/mobile/**").not().authenticated();
            http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS);
            http.logout().logoutUrl("/security/logout");
            http.csrf().disable();
        }

        @Bean
        public PasswordEncoder passwordEncoder() {
            return PasswordEncoderFactories.createDelegatingPasswordEncoder();
        }
    }
}
