package com.depment.ittest.database.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by Andrii Semenkov, PL on 14/07/2018.
 */
@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
public class Journal {
    @Id
    @Column
    private Long id;

    @CreatedDate
    @Column(nullable = false)
    private LocalDateTime eventCreationDate;

    @Column
    private String comment;

    @Column
    private String tags;
    @ManyToOne(optional = false,
            fetch = FetchType.EAGER,
            targetEntity = Environment.class)
    @JoinColumn(name = "userId",
            updatable = false,
            insertable = false,
            foreignKey = @ForeignKey(name = "EnvironmentInJournalFkey"),
            nullable = false)
    private Environment environment;
    @ManyToOne(optional = false,
            fetch = FetchType.EAGER,
            targetEntity = User.class)
    @JoinColumn(name = "userId",
            updatable = false,
            insertable = false,
            foreignKey = @ForeignKey(name = "UserInJournalFkey"),
            nullable = false)
    private User user;
}
