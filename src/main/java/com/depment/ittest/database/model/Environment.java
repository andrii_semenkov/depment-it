package com.depment.ittest.database.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Period;
import java.util.List;

/**
 * Created by Andrii Semenkov, PL on 11/06/2018.
 */
@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "environments")
public class Environment {
    @Id
    @Column
    @GeneratedValue
    private Long id;
    @Column
    private String nkey;
    @Column
    private String description;
    @ManyToOne
    @JoinColumn(name = "userId", foreignKey = @ForeignKey(name = "UserIdInEnvironmentFkey"))
    private User user;
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(foreignKey = @ForeignKey(name = "CategoryForInEnvironmentFkey"), nullable = false)
    private Category category;
    @Column
    private Period validityPeriod;
    @JoinColumn
    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<CategoryExtensionValue> categoryExtensionValues;
}
