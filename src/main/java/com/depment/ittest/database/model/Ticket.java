package com.depment.ittest.database.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Andrii Semenkov, PL on 10/06/2018.
 */
@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ticket")
public class Ticket {
    @Id
    @GeneratedValue
    @Column(unique = true, nullable = false)
    private Long id;
    @Lob
    @Column
    private String content;
    @Column(nullable = false)
    @LastModifiedDate
    private LocalDateTime date;
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Priority.class)
    @JoinColumn(name = "priorityId", foreignKey = @ForeignKey(name = "PriorityInConversationFkey"))
    private Priority priority;
    @ManyToOne(optional = false, fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name = "userId", foreignKey = @ForeignKey(name = "UserIdInConversFkey"), nullable = false)
    private User user;
    @JoinColumn
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Environment.class)
    private Environment environment;
    @Column
    private Boolean solved;
    @Column
    private String solution;
}
