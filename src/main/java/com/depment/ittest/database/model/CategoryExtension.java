package com.depment.ittest.database.model;

//import io.vavr.collection.List;

import com.depment.ittest.database.auditing.Auditable;
import lombok.*;

import javax.persistence.*;
import java.time.Period;

@Data
@Entity
@Builder
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(indexes = {@Index(name = "uniqueKeyFroCategory", columnList = "extensionKey, categoryFK", unique = true)})
public class CategoryExtension extends Auditable {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    @JoinColumn(nullable = false, name = "categoryFK")
    private Category category;
    @Column(nullable = false)
    private String extensionKey;
    @Column
    private Period validityPeriod;

    private CategoryExtension() {
    }


    public static CategoryExtension create(String key, Category category) {
        val extension = new CategoryExtension();
        extension.setCategory(category);
        extension.setExtensionKey(key);
        return extension;
    }
}
