package com.depment.ittest.database.model;

import com.depment.ittest.database.auditing.Auditable;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = true)
public class CategoryExtensionValue extends Auditable {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String extensionValue;
    @ManyToOne
    @JoinColumn(nullable = false)
    private CategoryExtension categoryExtension;

    private CategoryExtensionValue(final String extensionValue) {
        this.extensionValue = extensionValue;
    }

    public static CategoryExtensionValue of(String value) {
        return new CategoryExtensionValue(value);
    }

    public static CategoryExtensionValue of(String value, CategoryExtension category) {
        final CategoryExtensionValue categoryExtensionValue = new CategoryExtensionValue(value);
        categoryExtensionValue.setCategoryExtension(category);
        return categoryExtensionValue;
    }
}
