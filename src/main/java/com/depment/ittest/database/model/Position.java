package com.depment.ittest.database.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by Andrii Semenkov, PL on 10/06/2018.
 */
@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "positions",
        uniqueConstraints = {@UniqueConstraint(name = "PRIMARY", columnNames = Position.id_column)},
        indexes = {
                @Index(columnList = Position.name_column, unique = true, name = "name_UNIQUE"),
                @Index(columnList = Position.id_column, unique = true, name = "id_UNIQUE")
        })
public class Position {
    public static final String name_column = "positionName";
    public static final String id_column = "id";

    @Id
    @GeneratedValue
    @Column(nullable = false)
    private Long id;
    @Column(name = name_column,
            length = 45,
            unique = true,
            nullable = false)
    private String positionName;
    @Column(length = 144)
    private String description;
}
