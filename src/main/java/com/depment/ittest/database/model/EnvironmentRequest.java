package com.depment.ittest.database.model;

import lombok.*;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Example;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EnvironmentRequest {
    @OneToOne
    @JoinColumn(unique = true, nullable = false)
    private Environment requestedEnvironment;
    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;
    @Id
    @GeneratedValue
    private Long id;
    @Column
    @LastModifiedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date requestDate;
    @Column(nullable = false)
    private Boolean status = false;
    @Column
    private String declineReason;
    @Column
    private String requestReason;

    private EnvironmentRequest() {
    }

    private EnvironmentRequest(final Environment requestedEnvironment, final User user, final String requestReason) {
        this.requestDate = new Date();
        this.requestedEnvironment = requestedEnvironment;
        this.user = user;
        this.requestReason = requestReason;
    }

    public static EnvironmentRequest create(final Environment environment, final User user, final String requestReason) {
        return new EnvironmentRequest(environment, user, requestReason);
    }

    public static Example<EnvironmentRequest> createExampleForEmail(String userEmail) {
        final EnvironmentRequest environmentRequest = new EnvironmentRequest();
        environmentRequest.setUser(User.builder().email(userEmail).build());
        return Example.of(environmentRequest);
    }
}
