package com.depment.ittest.database.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users", uniqueConstraints = {@UniqueConstraint(columnNames = {"id", "email"})}, indexes = {@Index(name = "email_UNIQUE", unique = true, columnList = "email"), @Index(name = "email_indx", columnList = "email")})
public class User {

    public static final String id_column = "id";
    @Id
    @GeneratedValue
    @Column
    private Long id;
    @Column
    private String userName;
    @Column
    private String password;
    @Column(unique = true)
    private String employeeKey;
    @Column
    private String surname;
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "positionId", foreignKey = @ForeignKey(name = "PositionNameFKey"), nullable = false)
    private Position position;
    @Column
    private String email;
}
