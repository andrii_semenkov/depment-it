package com.depment.ittest.database;

import com.depment.ittest.database.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUserName(String name);

    //workaround not to break all things which already done. Caused by defect in initial dataset
    default Optional<User> findByEmail(String email) {
        return findByEmailOrUserName(email, email);
    }

    Optional<User> findByEmailOrUserName(String userName, String userEmail);
}
