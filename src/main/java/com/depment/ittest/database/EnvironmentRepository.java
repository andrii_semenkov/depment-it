package com.depment.ittest.database;

import com.depment.ittest.database.model.Environment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface EnvironmentRepository extends JpaRepository<Environment, Long> {
    Optional<Environment> findByNkeyAndCategory_CategoryName(String nkey, String categoryName);

    Optional<Environment> findByCategory_CategoryName(String categoryName);

    List<Environment> findByUser_Email(String email);

    List<Environment> findByUser_UserName(String userName);

    List<Environment> findByUser_Id(Long userId);
}
