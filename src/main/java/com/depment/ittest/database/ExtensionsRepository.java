package com.depment.ittest.database;

import com.depment.ittest.database.model.CategoryExtension;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExtensionsRepository extends JpaRepository<CategoryExtension, Long> {
}
