package com.depment.ittest.database;

import com.depment.ittest.database.model.EnvironmentRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnvironmentRequestRepository extends JpaRepository<EnvironmentRequest, Long> {
}
