package com.depment.ittest.database;

import com.depment.ittest.database.model.CategoryExtensionValue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExtensionValueRepository extends JpaRepository<CategoryExtensionValue, Long> {
}
