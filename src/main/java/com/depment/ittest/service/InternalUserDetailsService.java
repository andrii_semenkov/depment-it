package com.depment.ittest.service;

import com.depment.ittest.database.UserRepository;
import com.depment.ittest.security.MyUserPrincipal;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class InternalUserDetailsService implements UserDetailsService {
    public static final String BEAN_NAME = "InternalUserDetailsService";
    private final UserRepository userRepository;


    @Autowired
    public InternalUserDetailsService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        val user = userRepository.findByEmail(email);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException(email);
        }
        return new MyUserPrincipal(user.get());
    }
}
