package com.depment.ittest.service.interfaces;

import com.depment.ittest.database.model.EnvironmentRequest;

import java.util.List;

public interface EnvironmentRequestService {
    EnvironmentRequest createRequest(final Long environmentId, String userEmail, String requestReason);

    void acceptRequest(final Long requestId);

    void acceptRequest(final EnvironmentRequest environmentRequest);

    void declineRequest(final EnvironmentRequest environmentRequest, String reason);

    void declineRequest(final Long environmentRequestId, String reason);

    EnvironmentRequest find(final Long requestId);

    List<EnvironmentRequest> findByUserEmail(String userEmail);
}
