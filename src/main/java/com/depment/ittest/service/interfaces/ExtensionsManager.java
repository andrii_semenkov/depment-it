package com.depment.ittest.service.interfaces;

import com.depment.ittest.database.model.CategoryExtension;
import com.depment.ittest.database.model.CategoryExtensionValue;

import java.util.List;
import java.util.Optional;

public interface ExtensionsManager {
    CategoryExtension saveEmptyExtension(Long categoryId, String extensionKey);

    CategoryExtension saveExtension(Long categoryId, String extensionKey, String extensionValue);

    CategoryExtension saveExtension(CategoryExtension categoryExtension);

    void removeExtension(Long extensionId);

    void removeAll(Long categoryId);

    List<CategoryExtension> findAll(Long categoryId);

    List<CategoryExtension> findAll();

    Optional<CategoryExtension> findById(Long extensionId);

    CategoryExtension getExtension(Long extensionId);

    List<String> findExtensionValues(Long extensionId);

    CategoryExtension updateExtensionValue(Long extensionId, CategoryExtensionValue newValue);

    List<CategoryExtension> findAllByCategoryName(String categoryValue);
}
