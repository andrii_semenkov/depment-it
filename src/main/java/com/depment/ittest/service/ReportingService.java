package com.depment.ittest.service;

import com.depment.ittest.StupidWorkAroundDispatcher;
import com.depment.ittest.database.EnvironmentRepository;
import com.depment.ittest.database.TicketsRepository;
import com.depment.ittest.database.UserRepository;
import com.depment.ittest.database.model.Environment;
import com.depment.ittest.database.model.Ticket;
import com.depment.ittest.database.model.User;
import lombok.val;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ReportingService {
    private final TicketsRepository ticketsRepository;
    private final UserRepository userRepository;
    private final EnvironmentRepository environmentRepository;

    public ReportingService(final TicketsRepository ticketsRepository, final UserRepository userRepository, final EnvironmentRepository environmentRepository) {
        this.ticketsRepository = ticketsRepository;
        this.userRepository = userRepository;
        this.environmentRepository = environmentRepository;
    }

    public void reportEnvironmentProblem(String userEmail, List<Long> environmentsIds, String problemDescription) {
        final Optional<User> optionalUser = userRepository.findByEmail(userEmail);
        final List<Environment> environments = environmentRepository.findAllById(environmentsIds);
        if (!optionalUser.isPresent() || environments.isEmpty()) {
            throw new IllegalArgumentException("User fo reporting problem did not found.");
        }
        environments.forEach(environment -> ticketsRepository.saveAndFlush(Ticket.builder().content(problemDescription).user(optionalUser.get()).solved(false).date(LocalDateTime.now()).environment(environment).build()));

    }

    public List<Ticket> findAllReportedForUser(String useremail) {
        //stupid workaround. It is not my fault...
        val collectedResult = new HashSet<Ticket>();
        StupidWorkAroundDispatcher.createForTicket(useremail).forEach(example -> collectedResult.addAll(ticketsRepository.findAll(example)));
        return new ArrayList<>(collectedResult);
    }

    public Long countAllReported() {
        return ticketsRepository.count(Example.of(Ticket.builder().solved(false).build()));
    }

    public List<Ticket> findAll() {
        return ticketsRepository.findAll();
    }

    public void saveTickets(final List<Ticket> tickets) {
        this.ticketsRepository.saveAll(tickets);
        this.ticketsRepository.flush();
    }

    public void saveSolution(final Long id, final String solution) {
        final Optional<Ticket> byId = this.ticketsRepository.findById(id);
        if (byId.isPresent()) {
            final Ticket ticket = byId.get();
            ticket.setSolution(solution);
            ticket.setSolved(true);
            this.ticketsRepository.saveAndFlush(ticket);
        }
    }
}
