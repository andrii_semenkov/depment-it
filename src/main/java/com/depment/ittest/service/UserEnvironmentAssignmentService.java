package com.depment.ittest.service;

import com.depment.ittest.database.EnvironmentRepository;
import com.depment.ittest.database.UserRepository;
import com.depment.ittest.database.model.Environment;
import com.depment.ittest.database.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class UserEnvironmentAssignmentService {
    private UserRepository userRepository;
    private EnvironmentRepository environmentRepository;

    @Autowired
    public UserEnvironmentAssignmentService(final UserRepository userRepository, final EnvironmentRepository environmentRepository) {
        this.userRepository = userRepository;
        this.environmentRepository = environmentRepository;
    }

    public void addToUser(final Long userId, List<Long> envIds) {
        final List<Environment> byUser_id = environmentRepository.findByUser_Id(userId);
        byUser_id.forEach(environment -> environment.setUser(null));
        environmentRepository.saveAll(byUser_id);
        environmentRepository.flush();

        final List<Environment> environments = environmentRepository.findAllById(envIds);
        final Optional<User> user = userRepository.findById(userId);
        if (!environments.isEmpty() && user.isPresent()) {
            environments.forEach(environment -> environment.setUser(user.get()));
            environmentRepository.saveAll(environments);
            environmentRepository.flush();
        } else if (environments.isEmpty()) {
            environments.forEach(environment -> environment.setUser(null));
            environmentRepository.saveAll(environments);
            environmentRepository.flush();
        } else {
            throw new IllegalArgumentException("Not found valid data for environemnt {0} and user {1}." + envIds + "   " + userId);
        }
    }


    public void addToUser(final Long userId, final Long envId) {
        final Optional<Environment> environment = environmentRepository.findById(envId);
        final Optional<User> user = userRepository.findById(userId);

        if (environment.isPresent() && user.isPresent()) {
            final Environment found = environment.get();
            found.setUser(user.get());
            environmentRepository.saveAndFlush(found);
        } else {
            throw new IllegalArgumentException("Not found valid data for environemnt {0} and user {1}." + envId + "   " + userId);
        }
    }

    public void unAssignEnvironment(final Long envId) {
        final Optional<Environment> environment = environmentRepository.findById(envId);
        if (environment.isPresent()) {
            final Environment found = environment.get();
            found.setUser(null);
            environmentRepository.saveAndFlush(found);
        } else {
            throw new IllegalArgumentException("Not found valid data for environemnt {0} " + envId);
        }
    }

}
