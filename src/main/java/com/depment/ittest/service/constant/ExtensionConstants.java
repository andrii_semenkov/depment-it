package com.depment.ittest.service.constant;

public interface ExtensionConstants {
    String PROCESSOR = "Processor";
    String COLOR = "COLOR";
    String SIZE = "SIZE";
    String COST = "COST";
    String GRAFICS = "GRAFICS";
}
