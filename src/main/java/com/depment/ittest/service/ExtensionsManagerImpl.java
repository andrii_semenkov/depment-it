package com.depment.ittest.service;

import com.depment.ittest.database.CategoryRepository;
import com.depment.ittest.database.ExtensionValueRepository;
import com.depment.ittest.database.ExtensionsRepository;
import com.depment.ittest.database.model.Category;
import com.depment.ittest.database.model.CategoryExtension;
import com.depment.ittest.database.model.CategoryExtensionValue;
import com.depment.ittest.service.interfaces.ExtensionsManager;
import lombok.val;
import org.apache.commons.lang3.Validate;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ExtensionsManagerImpl implements ExtensionsManager {
    private final ExtensionsRepository extensionsRepository;
    private final CategoryRepository categoryRepository;
    private final ExtensionValueRepository extensionValueRepository;

    public ExtensionsManagerImpl(final ExtensionsRepository extensionsRepository, final CategoryRepository categoryRepository, final ExtensionValueRepository extensionValueRepository) {
        this.extensionsRepository = extensionsRepository;
        this.categoryRepository = categoryRepository;
        this.extensionValueRepository = extensionValueRepository;
    }

    @Override
    public CategoryExtension saveEmptyExtension(final Long categoryId, final String extensionKey) {
        return extensionsRepository.saveAndFlush(CategoryExtension.builder().category(categoryRepository.findById(categoryId).get()).extensionKey(extensionKey).build());
    }

    @Override
    public CategoryExtension saveExtension(final Long categoryId, String extensionKey, String extensionValue) {
        val category = categoryRepository.findById(categoryId);
        CategoryExtension extension = CategoryExtension.create(extensionKey, category.get());
        return extensionsRepository.saveAndFlush(extension);
    }

    @Override
    public CategoryExtension saveExtension(final CategoryExtension categoryExtension) {
        Validate.notNull(categoryExtension);
        return extensionsRepository.saveAndFlush(categoryExtension);
    }

    @Override
    public void removeExtension(final Long extensionId) {
        extensionsRepository.deleteById(extensionId);
    }

    @Override
    public void removeAll(final Long categoryId) {
        val example = Example.of(CategoryExtension.builder().category(Category.builder().id(categoryId).build()).build());
        final List<CategoryExtension> categoryExtensions = extensionsRepository.findAll(example);
        categoryExtensions.forEach(extension -> {
            final List<CategoryExtensionValue> all = extensionValueRepository.findAll(Example.of(CategoryExtensionValue.builder().categoryExtension(extension).build()));
            extensionValueRepository.deleteInBatch(all);
        });
        extensionValueRepository.flush();
        extensionsRepository.deleteInBatch(categoryExtensions);
        extensionsRepository.flush();

    }

    @Override
    public List<CategoryExtension> findAll(final Long categoryId) {
        val extensionExample = Example.of(CategoryExtension.builder().category(Category.builder().id(categoryId).build()).build());
        return extensionsRepository.findAll(extensionExample);
    }

    @Override
    public List<CategoryExtension> findAll() {
        return extensionsRepository.findAll();
    }

    @Override
    public Optional<CategoryExtension> findById(final Long extensionId) {
        return extensionsRepository.findById(extensionId);
    }

    @Override
    public CategoryExtension getExtension(final Long extensionId) {
        return extensionsRepository.findById(extensionId).get();
    }

    @Override
    public List<String> findExtensionValues(final Long extensionId) {
        final Optional<CategoryExtension> extensionsRepositoryById = extensionsRepository.findById(extensionId);
        if (extensionsRepositoryById.isPresent()) {
            return extensionValueRepository.findAll(Example.of(CategoryExtensionValue.
                    builder().
                    categoryExtension(extensionsRepositoryById.get()).
                    build())).stream().map(CategoryExtensionValue::getExtensionValue).collect(Collectors.toList());
        } else {
            return emptyList();
        }

    }

    @Override
    public CategoryExtension updateExtensionValue(final Long extensionId, final CategoryExtensionValue newValue) {
        final Optional<CategoryExtension> extension = extensionsRepository.findById(extensionId);
        if (extension.isPresent()) {
            val extens = extension.get();
//            newValue.setId(new JdkIdGenerator().generateId().getMostSignificantBits());
            extensionsRepository.saveAndFlush(extens);
        }
        return null;
    }

    @Override
    public List<CategoryExtension> findAllByCategoryName(final String categoryValue) {
        val example = Example.of(CategoryExtension.builder().category(Category.builder().categoryName(categoryValue).build()).build());
        return extensionsRepository.findAll(example);
    }
}
