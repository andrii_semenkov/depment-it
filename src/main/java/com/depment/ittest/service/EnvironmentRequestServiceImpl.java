package com.depment.ittest.service;

import com.depment.ittest.StupidWorkAroundDispatcher;
import com.depment.ittest.database.EnvironmentRepository;
import com.depment.ittest.database.EnvironmentRequestRepository;
import com.depment.ittest.database.UserRepository;
import com.depment.ittest.database.model.EnvironmentRequest;
import com.depment.ittest.service.interfaces.EnvironmentRequestService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@Service
@Transactional
public class EnvironmentRequestServiceImpl implements EnvironmentRequestService {
    private final EnvironmentRequestRepository environmentRequestRepository;
    private final EnvironmentRepository environmentRepository;
    private final UserRepository userRepository;

    @Autowired
    public EnvironmentRequestServiceImpl(final EnvironmentRequestRepository environmentRequestRepository, final EnvironmentRepository environmentRepository, final UserRepository userRepository) {
        this.environmentRequestRepository = environmentRequestRepository;
        this.environmentRepository = environmentRepository;
        this.userRepository = userRepository;
    }

    public EnvironmentRequest createRequest(Long environmentId, String userEmail, String requestReason) {
        val user = userRepository.findByEmail(userEmail);
        val environement = environmentRepository.findById(environmentId);
        if (user.isPresent() && environement.isPresent()) {
            return environmentRequestRepository.saveAndFlush(EnvironmentRequest.create(environement.get(), user.get(), requestReason));
        } else {
            throw new IllegalArgumentException("User or Environment for request is not found");
        }
    }

    @Override
    public void acceptRequest(final Long requestId) {
        val request = environmentRequestRepository.findById(requestId);
        if (request.isPresent()) {
            final EnvironmentRequest environmentRequest = request.get();
            environmentRequest.setStatus(true);
            environmentRequestRepository.saveAndFlush(environmentRequest);
        } else {
            throw new IllegalArgumentException("Request not found. Id: " + requestId.toString());
        }
    }

    @Override
    public void acceptRequest(final EnvironmentRequest environmentRequest) {
        if (environmentRequestRepository.existsById(environmentRequest.getId())) {
            environmentRequest.setStatus(true);
            environmentRequestRepository.saveAndFlush(environmentRequest);
        } else {
            throw new IllegalArgumentException("Request not found. Id: " + environmentRequest.getId());
        }
    }

    @Override
    public void declineRequest(final EnvironmentRequest environmentRequest, final String reason) {
        if (environmentRequestRepository.existsById(environmentRequest.getId())) {
            environmentRequest.setStatus(false);
            environmentRequest.setDeclineReason(reason);
            environmentRequestRepository.saveAndFlush(environmentRequest);
        } else {
            throw new IllegalArgumentException("Request not found. Id: " + environmentRequest.getId());
        }
    }

    @Override
    public void declineRequest(final Long environmentRequestId, final String reason) {
        val request = environmentRequestRepository.findById(environmentRequestId);
        if (request.isPresent()) {
            final EnvironmentRequest environmentRequest = request.get();
            environmentRequest.setStatus(true);
            environmentRequest.setDeclineReason(isEmpty(reason) ? null : reason);
            environmentRequestRepository.saveAndFlush(environmentRequest);
        } else {
            throw new IllegalArgumentException("Request not found. Id: " + environmentRequestId);
        }
    }

    @Override
    public EnvironmentRequest find(final Long requestId) {
        return environmentRequestRepository.findById(requestId).orElseThrow(() -> new IllegalArgumentException("Request for provided id not found : " + requestId));
    }

    @Override
    public List<EnvironmentRequest> findByUserEmail(final String userEmail) {
        val collected = new HashSet<EnvironmentRequest>();
        StupidWorkAroundDispatcher.createForUser(userEmail).forEach(example -> collected.addAll(environmentRequestRepository.findAll(example)));
        return new ArrayList<>(collected);
    }
}
