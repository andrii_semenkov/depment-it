package com.depment.ittest.service;

import com.depment.ittest.database.ToDoListRepository;
import com.depment.ittest.database.model.ToDoListItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ToDoListArchivingService {
    private ToDoListRepository repository;

    @Autowired
    public ToDoListArchivingService(final ToDoListRepository toDoListRepository) {
        this.repository = toDoListRepository;
    }

    @Scheduled(fixedRate = 300000)
    public void archiveChecked() {
        ToDoListItem item = new ToDoListItem();
        item.setChecked(true);
        Example<ToDoListItem> example = Example.of(item);
        List<ToDoListItem> items = repository.findAll(example);
        items.forEach(itm -> itm.setArchived(true));
        repository.saveAll(items);
        repository.flush();
    }


}
