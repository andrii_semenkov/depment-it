package com.depment.ittest.service;

import com.depment.ittest.database.CategoryRepository;
import com.depment.ittest.database.EnvironmentRepository;
import com.depment.ittest.database.ExtensionValueRepository;
import com.depment.ittest.database.UserRepository;
import com.depment.ittest.database.model.Category;
import com.depment.ittest.database.model.CategoryExtension;
import com.depment.ittest.database.model.CategoryExtensionValue;
import com.depment.ittest.database.model.Environment;
import com.depment.ittest.service.constant.ExtensionConstants;
import com.depment.ittest.service.environment.AbstractTestEnvironment;
import com.depment.ittest.service.interfaces.ExtensionsManager;
import lombok.val;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED)
public class ExtensionsManagerTest extends AbstractTestEnvironment {

    @Autowired
    ExtensionsManager extensionsManager;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    EnvironmentRepository environmentRepository;
    @Autowired
    ExtensionValueRepository extensionValueRepository;


    @Test
    public void saveExtension() {
        val extension = createAndSave();
        Assert.assertNotNull(extension);
    }

    @Test
    public void shouldSaveValuesForExtention() {
        final Category category = categoryRepository.findAll().get(0);
        val extension = extensionsManager.findAllByCategoryName(category.getCategoryName()).get(0);
        Environment environment = Environment.builder()
                .description("description")
                .nkey("nkey")
                .user(userRepository.findAll().get(0))
                .category(category)
                .categoryExtensionValues(Lists.newArrayList(CategoryExtensionValue.of("newValue", extension)))
                .build();
        environmentRepository.saveAndFlush(environment);

        Assert.assertNotNull(extensionsManager.findExtensionValues(extension.getId()));
        Assert.assertEquals(extensionsManager.findExtensionValues(extension.getId()).get(0), "newValue");
        //Test nothing due 2 transaction management issue, have not time to fix it.
    }

    @Test
    public void saveExtension1() {
        val categoryId = getCategory().getId();
        val extensionValue = "i3-5500U";
        val extension = extensionsManager.saveExtension(categoryId, getExtensionValue(), extensionValue);
        Assert.assertNotNull(extension);
    }

    private String getExtensionValue() {
        return ExtensionConstants.PROCESSOR + new Random(100).toString();
    }

    @Test
    public void saveEmptyExtension1() {
        val categoryId = getCategory().getId();
//        val extensionValue = "i3-5500U";
        val extension = extensionsManager.saveEmptyExtension(categoryId, getExtensionValue());
        Assert.assertNotNull(extension);
    }

    @Test
    public void removeExtension() {
        val extension = createAndSave();
        extensionsManager.removeExtension(extension.getId());
        val deleted = extensionsManager.findById(extension.getId());
        Assert.assertFalse(deleted.isPresent());
    }

    @Test
    public void removeAll() {
        val extension = createAndSave();
        final Long categoryId = extension.getCategory().getId();
        extensionsManager.removeAll(categoryId);
        Assert.assertTrue(extensionsManager.findAll().stream().noneMatch(categoryExtension -> categoryExtension.getCategory().getId().equals(categoryId)));
    }

    @Test
    public void findAll() {
        createAndSave();
        val extensions = extensionsManager.findAll();
        Assert.assertTrue(extensions.size() > 0);
    }

    @Test
    public void getExtension() {
        val extension = createAndSave();
        val stored = extensionsManager.getExtension(extension.getId());
        Assert.assertEquals(stored.getId(), extension.getId());
    }

//    @Test
//    public void findExtensionValues() {
//        val extension = createAndSave();
//        val values = extensionsManager.findExtensionValues(extension.getId());
//        Assert.assertFalse(values.isEmpty());
//    }

//    @Test
//    public void updateExtensionValue() {
//        val extention = createAndSave();
//        final CategoryExtensionValue extensionValue = extention.getExtensionValues().stream().findFirst().get();
//        final CategoryExtensionValue blabla = CategoryExtensionValue.of("Blabla");
//        extensionsManager.updateExtensionValue(extention.getId(), blabla);
//        val updated = extensionsManager.findExtensionValues(extention.getId());
//        Assert.assertTrue(updated.contains(blabla.getExtensionValue()));
//    }

    @Test
    public void findAllByCategoryName() {
        val extention = createAndSave();
        val stored = extensionsManager.findAllByCategoryName(extention.getCategory().getCategoryName());
//        Assert.assertTrue(stored.get(0).equals(extention));
    }

    private CategoryExtension createAndSave() {
        val category = getCategory();

        val values = createValues("i7-8700K");

        return extensionsManager.saveExtension(CategoryExtension.builder().category(category).extensionKey(getExtensionValue()).build());
    }

    private List<CategoryExtensionValue> createValues(String... values) {
        return Stream.of(values).map(CategoryExtensionValue::of).collect(Collectors.toList());
    }

    private Category getCategory() {
        return categoryRepository.findAll().stream().findFirst().get();
    }
}