package com.depment.ittest.service;

import com.depment.ittest.database.EnvironmentRepository;
import com.depment.ittest.database.TicketsRepository;
import com.depment.ittest.database.UserRepository;
import com.depment.ittest.database.model.Ticket;
import com.depment.ittest.service.environment.AbstractTestEnvironment;
import lombok.val;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


public class ReportingServiceTest extends AbstractTestEnvironment {

    @Autowired
    ReportingService reportingService;
    @Autowired
    EnvironmentRepository environmentRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    TicketsRepository conversationRepository;

    @Test
    public void reportEnvironmentProblem() {
        val environment = environmentRepository.findAll().stream().filter(env -> env.getUser() != null).findFirst().get();

        final String some_description = "Some description";
        reportingService.reportEnvironmentProblem(environment.getUser().getEmail(), Lists.newArrayList(environment.getId()), some_description);

        final Ticket ticket = conversationRepository.findAll().get(0);

        Assert.assertEquals(ticket.getContent(), some_description);
    }
}