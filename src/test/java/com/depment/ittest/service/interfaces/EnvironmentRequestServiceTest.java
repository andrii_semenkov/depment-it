package com.depment.ittest.service.interfaces;

import com.depment.ittest.database.EnvironmentRepository;
import com.depment.ittest.database.model.Environment;
import com.depment.ittest.database.model.EnvironmentRequest;
import com.depment.ittest.service.environment.AbstractTestEnvironment;
import lombok.val;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class EnvironmentRequestServiceTest extends AbstractTestEnvironment {

    static final String user = "user";
    @Autowired
    private EnvironmentRequestService environmentRequestService;
    @Autowired
    private EnvironmentRepository environmentRepository;

    @Test
    public void createRequest() {
        val environment = getEnvironment();

        val request = environmentRequestService.createRequest(environment.getId(), user, "");

        Assert.assertNotNull(request);
    }

    @Test
    public void findByEmailTest() {
        val environment = getEnvironment();

        environmentRequestService.createRequest(environment.getId(), user, "");

        val stored = environmentRequestService.findByUserEmail(user);

        Assert.assertEquals(stored.stream().findFirst().get().getUser().getEmail(), user);
    }


    @Test
    public void acceptRequest() {
        val request = createEnvRequest();

        environmentRequestService.acceptRequest(request);

        val stored = environmentRequestService.find(request.getId());

        Assert.assertEquals(stored.getStatus(), true);
    }

    @Test
    public void acceptRequest1() {
    }

    @Test
    public void declineRequest() {
        val request = createEnvRequest();

        environmentRequestService.declineRequest(request, null);

        val stored = environmentRequestService.find(request.getId());

        Assert.assertEquals(stored.getStatus(), false);
    }

    @Test
    public void declineRequest1() {
        val request = createEnvRequest();

        final String reason = "reason";
        environmentRequestService.declineRequest(request, reason);

        val stored = environmentRequestService.find(request.getId());

        Assert.assertEquals(stored.getStatus(), false);
        Assert.assertEquals(stored.getDeclineReason(), reason);
    }

    private Environment getEnvironment() {
        return environmentRepository.findAll().stream().findFirst().get();
    }

    private EnvironmentRequest createEnvRequest() {
        val environment = getEnvironment();

        return environmentRequestService.createRequest(environment.getId(), user, "");
    }
}