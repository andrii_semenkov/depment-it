package com.depment.ittest.service;

import com.depment.ittest.database.EnvironmentRepository;
import com.depment.ittest.database.UserRepository;
import com.depment.ittest.service.environment.AbstractTestEnvironment;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
//
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserEnvironmentAssignmentServiceTest {
    @Autowired
    private UserEnvironmentAssignmentService assignmentService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EnvironmentRepository environmentRepository;


    @Test
    public void addToUser() {
        Long userId = userRepository.findAll().stream().findFirst().get().getId();
        Long envId = environmentRepository.findAll().stream().findFirst().get().getId();

        assignmentService.addToUser(userId, envId);

        Assert.assertEquals(environmentRepository.findById(envId).get().getUser().getId(), userId);
    }

    @Test
    public void should_remove_environment_from_user() {
        Long envId = environmentRepository.findAll().stream().filter(environment -> environment.getUser() != null).findFirst().get().getId();

        assignmentService.unAssignEnvironment(envId);

        Assert.assertNull(environmentRepository.findById(envId).get().getUser());
    }
}