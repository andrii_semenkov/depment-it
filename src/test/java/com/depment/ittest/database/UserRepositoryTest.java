package com.depment.ittest.database;

import com.depment.ittest.database.model.User;
import com.depment.ittest.service.environment.AbstractTestEnvironment;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class UserRepositoryTest extends AbstractTestEnvironment {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void shouldFindUserByNameOrEmail() {
        final Optional<User> first = userRepository.findAll().stream().findFirst();

        final String userName = first.get().getUserName();
        final Optional<User> byEmailOrUserName = userRepository.findByEmailOrUserName(userName, userName);
        final String email = first.get().getEmail();
        final Optional<User> byEmailOrUserName1 = userRepository.findByEmailOrUserName(email, email);

        Assert.assertEquals(first, byEmailOrUserName);
        Assert.assertEquals(first, byEmailOrUserName1);

    }

}